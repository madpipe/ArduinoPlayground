#include "Motor.h"

const int speed = 1000;
Motor motor1(7, 8);

const int totalSteps = 360/1.8;
const int oneSecondSteps = totalSteps / 60;
const int interStepDelay = 5;

int currentSecond = 0;

const int button = 2;

void setup() {
  pinMode(button, INPUT);
}

void loop() {
  if (digitalRead(button)) {
    runOneMinute();
  }
}

void runOneMinute() {
  for (int i = 0; i < 60; i++) {
    int minus = 0;
    if (i%oneSecondSteps == 0) {
      motor1.step();
      delay(interStepDelay);
    }
    stepOneSecond(minus);
  }
}

void stepOneSecond(int minus) {
  int d = 1000 - minus;
  
  for (int i = 0; i < oneSecondSteps; i++) {
    motor1.step();
    delay(interStepDelay);
    d -= interStepDelay;
  }
  delay(d);
}

