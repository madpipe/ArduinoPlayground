#include "Motor.h"
#include "Arduino.h"

Motor::Motor(int pinA, int pinB) : pinA(pinA), pinB(pinB) {
  currentDirection = FORWARD;
  currentState = STEP_1;

  pinMode(pinA, OUTPUT);
  pinMode(pinB, OUTPUT);
}

void Motor::move(int steps, int speed) {
  move(steps, speed, currentDirection);
}

void Motor::move(int steps, int speed, bool direction) {
  for (int i = 1; i <=steps; i++) {
    step(direction);
    delay(speed);
  }
}

void Motor::step() {
  step(currentDirection);
}

void Motor::step(bool direction) {
  if (direction == FORWARD) {
    switch (currentState) {
      case STEP_1:
        step2();
        break;
      case STEP_2:
        step3();
        break;
      case STEP_3:
        step4();
        break;
      case STEP_4:
        step1();
        break;
    }
  }
  else if (direction == BACKWARD) {
    switch (currentState) {
      case STEP_1:
        step4();
        break;
      case STEP_2:
        step1();
        break;
      case STEP_3:
        step2();
        break;
      case STEP_4:
        step3();
        break;
    }
  }
}

void Motor::setDirection(bool direction) {
  currentDirection = direction;
}

void Motor::step1() {
  digitalWrite(pinA, LOW);
  digitalWrite(pinB, LOW);
  currentState = STEP_1;
}
void Motor::step2() {
  digitalWrite(pinA, LOW);
  digitalWrite(pinB, HIGH);
  currentState = STEP_2;
}
void Motor::step3() {
  digitalWrite(pinA, HIGH);
  digitalWrite(pinB, HIGH);
  currentState = STEP_3;
}
void Motor::step4() {
  digitalWrite(pinA, HIGH);
  digitalWrite(pinB, LOW);
  currentState = STEP_4;
}

