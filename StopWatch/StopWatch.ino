// Using this Stepper Library: https://github.com/laurb9/StepperDriver

#include <Arduino.h>
#include "DRV8825.h"

#define MOTOR_STEPS 200

#define DIR 8
#define STEP 9
#define ENBL 7
#define M0 10
#define M1 11
#define M2 12

// Set current microstep level, 1=full speed, 32=fine microstepping
// Options: 1, 2, 4, 8, 16, 32
#define MICROSTEPS 32

DRV8825 stepper(MOTOR_STEPS, DIR, STEP, ENBL, M0, M1, M2);

void setup() {
    stepper.setRPM(100);
    stepper.setMicrostep(MICROSTEPS);
}

void loop() {
    // energize coils - the motor will hold position
    stepper.enable();
    
    stepper.rotate(360);
    
    stepper.disable();
    
    delay(1000);
}
