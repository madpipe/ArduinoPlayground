#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

const int rows = 2, columns = 16;
//	            I   F   M   A   M   I   I   A   S   O   N   D
int months[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
String weekDays[] = {"Lun", "Mar", "Mie", "Joi", "Vin", "Sam", "Dum"};


int day 	= 25;
int month 	= 7;
int year 	= 2014;
int hour	= 23;
int minute	= 35;
int second	= 20;
int weekDay	= 4;
String date;
String time;

void updateTime()
{
	second++;
	if (second > 59)
	{
		second = 0;
		minute++;
		if (minute > 59)
		{
			minute = 0;
			hour ++;
			if (hour > 23)
			{
				hour = 0;
				day++;
				weekDay++;
				if (weekDay > 6)
					weekDay = 0;
				if (day > months[month-1])
				{
					day = 1;
					month++;
					if (month > 12)
					{
						month = 1;
						year++;
					}
				}
			}
		}
	}
}

void centerString(String* str)
{
	if (str->length() <= columns)
	{
		int space = columns - str->length();
		if(space > 0)
		{
			if ((space%2) == 0)
				space = space/2;
			else
				space = space/2 - 1;
			for(int i = 0; i < space; i++)
				*str = String(" ") + *str;
		}
	}
}

String subTen(int nr)
{
	String x = nr + String("");
	if (nr < 10)
		x = String("0") + nr;
	return x;
}

void updateView()
{
	lcd.clear();
	
	lcd.setCursor(0, 0); // Print on first row
	if (month < 10)
	date = year + String(".") + subTen(month) + String(".") + subTen(day) + String(" ") 
																	+ weekDays[weekDay];
	centerString(&date);
	lcd.print(date);
	
	lcd.setCursor(0, rows-1); // Print on last row
	time = subTen(hour) + String(":") + subTen(minute) + String(":") + subTen(second);
	centerString(&time);
	lcd.print(time);
}

void setup() {
	// set up the LCD's number of columns and rows: 
	lcd.begin(columns, rows);
	updateView();
}

void loop() {
	delay(1050);
	updateTime();
	updateView();
}
