/*
http://s3.media.squarespace.com/production/1275805/14967857/blog/wp-content/uploads/2011/11/Arduino_Hello_World_NG0R_2.png
http://www.hackmeister.dk/2010/08/4-lcd-displays-on-1-arduino/
*/
// include the library code:
#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd1(12, 5, 7, 8, 9, 10);
LiquidCrystal lcd2(12, 4, 7, 8, 9, 10);
LiquidCrystal lcd3(12, 3, 7, 8, 9, 10);
LiquidCrystal lcd4(12, 2, 7, 8, 9, 10);

void setup() {
  // set up the LCD's number of columns and rows: 
  lcd1.begin(16, 2);
  lcd2.begin(16, 2);
  lcd3.begin(16, 2);
  lcd4.begin(16, 2);
  // Print a message to the LCD.
  lcd1.print("Laura");
  lcd2.print("sa stii");
  lcd3.print("ca eu");
  lcd4.print("TE IUBESC");
}

void loop() {
  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
//  lcd1.setCursor(0, 1);
//  lcd2.setCursor(0, 1);
//  lcd3.setCursor(0, 1);
//  lcd4.setCursor(0, 1);
  // print the number of seconds since reset:
//  lcd1.print(millis()/1000);
//  lcd2.print(millis()/1000);
//  lcd3.print(millis()/1000);
//  lcd4.print(millis()/1000);
}

