/* PIN LAYOUT
I2C <-> Arduino
-----------------
GND - GND
VCC - 5+V
SDA - A4
SCL - A5

I2C <-> LCD
-----------------
I2C Pin 1 is the one next to GND
*/
/* Help from
http://arduino-info.wikispaces.com/LCD-Blue-I2C
http://www.instructables.com/id/I2C-LCD-Controller-the-easy-way/?ALLSTEPS
*/

#include <Wire.h>
#include <LiquidCrystal_I2C.h>  // F Malpartida's NewLiquidCrystal library

#define I2C_ADDR    0x27  // Define I2C Address for controller
#define En_pin  2
#define Rw_pin  1
#define Rs_pin  0
#define D4_pin  4
#define D5_pin  5
#define D6_pin  6
#define D7_pin  7
#define Bl_pin  3

LiquidCrystal_I2C  lcd(I2C_ADDR,En_pin,Rw_pin,Rs_pin,D4_pin,D5_pin,D6_pin,D7_pin,Bl_pin,POSITIVE);

void setup() 
{
  lcd.begin (16,2);  // initialize the lcd
}

void loop()  
{
// Print on the LCD
  lcd.backlight();  
  lcd.setCursor(0,0); 
  lcd.print("Te iubesc Laura!");
  delay(8000);
}
