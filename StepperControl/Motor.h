#ifndef _MOTOR_H_
#define _MOTOR_H_

#define FORWARD true
#define BACKWARD false

class Motor {
public:
  Motor(int pinA, int pinB);

  void step();
  void step(bool direction);

  void move(int steps, int speed);
  void move(int steps, int speed, bool direction);

  void setDirection(bool direction);

private:
  const int pinA;
  const int pinB;
  bool currentDirection;

  enum State{
    STEP_1,
    STEP_2,
    STEP_3,
    STEP_4
  };

  State currentState;

  void step1();
  void step2();
  void step3();
  void step4();
};

#endif // _MOTOR_H_
